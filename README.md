A board game of colonization, negotiation, and competition.

If you want to see how the game is played, check out the PDF of the [manual](https://bytebucket.org/davidystephenson/empirespace/raw/c952ca0be737867918f567f545c450ec13bbb839/manual.pdf).

If you're interested in contacting the designers, you can reach them at david@davidystephenson.com.
